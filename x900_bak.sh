#!/bin/bash
blue=`tput setaf 4`
green=`tput setaf 2`
reset=`tput sgr0`
echo "${blue}°°°·.°·..·°¯°·._.·        © 𝕤𝕒𝕟𝕥𝕚𝕒𝕘𝕠𝕝𝕒𝕓         ·._.·°¯°·.·° .·°°°"
echo "°°°·.°·..·°¯°·._.· Script Convertir PDF a Imagenes tamaño x900 ·._.·°¯°·.·° .·°°°"
echo ""
echo "${reset}Procesando... Por favor espere..."

convert -alpha remove -density 300 -shave 50x50 -resize x900 -gravity center -scene 1 -intent relative -black-point-compensation -colorspace sRGB revista.pdf[0-43] x900/%d.jpg

#convert -alpha remove -density 300 -shave 230x160 -resize x900 -gravity center -scene 9 -intent relative -black-point-compensation -colorspace sRGB revista.pdf[8] x900/%d.jpg

# pagina 1
#convert -alpha remove -density 300 +repage -crop 52%x100%+2700+0 -shave 130x160 +repage -resize x900 -scene 1 -intent relative -black-point-compensation -colorspace sRGB revista.pdf[0] x900/%d.jpg

# pagina 2
#convert -alpha remove -density 300 +repage -crop 52%x100%-20+0 -shave 150x170 +repage -resize x900 -scene 2 -intent relative -black-point-compensation -colorspace sRGB revista.pdf[1] x900/%d.jpg

# pagina 43
#convert -alpha remove -density 300 +repage -crop 52%x100%+2700+0 -shave 150x210 +repage -resize x900 -scene 43 -intent relative -black-point-compensation -colorspace sRGB revista.pdf[42] x900/%d.jpg

# pagina 44
#convert -alpha remove -density 300 +repage -crop 52%x100%-20+0 -shave 130x160 +repage -resize x900 -scene 44 -intent relative -black-point-compensation -colorspace sRGB revista.pdf[43] x900/%d.jpg

# pagina 6
#convert -alpha remove -density 300 -shave 230x180 -resize x900 -gravity center -scene 6 -intent relative -black-point-compensation -colorspace sRGB revista.pdf[5] x900/%d.jpg

#pagina 7
#convert -alpha remove -density 300 -shave 160x160 -resize x900 -gravity center -scene 7 -intent relative -black-point-compensation -colorspace sRGB revista.pdf[6] x900/%d.jpg

# pagina 8
#convert -alpha remove -density 300 -shave 230x160 -resize x900 -gravity center -scene 8 -intent relative -black-point-compensation -colorspace sRGB revista.pdf[7] x900/%d.jpg

# pagina 9
#convert -alpha remove -density 300 -shave 230x160 -resize x900 -gravity center -scene 9 -intent relative -black-point-compensation -colorspace sRGB revista.pdf[8] x900/%d.jpg
