#!/bin/bash
blue=`tput setaf 4`
green=`tput setaf 2`
reset=`tput sgr0`
echo "${blue}°°°·.°·..·°¯°·._.·        © 𝕤𝕒𝕟𝕥𝕚𝕒𝕘𝕠𝕝𝕒𝕓         ·._.·°¯°·.·° .·°°°"
echo "°°°·.°·..·°¯°·._.· Script Convertir PDF a Imagenes tamaño x900 ·._.·°¯°·.·° .·°°°"
echo ""
echo "${reset}Procesando... Por favor espere..."

convert -alpha remove -density 300 -shave 150x150 -resize x900 -gravity center -scene 1 -intent relative -black-point-compensation -colorspace sRGB revista.pdf[0-43] x900/%d.jpg