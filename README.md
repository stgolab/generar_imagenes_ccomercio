#PROCESAR IMAGENES CAMARA DE COMERCIO ##


#requisitos:
1-ImageMagick 7.0.7-22
2-Sistema Operativo Linux / iOS 

#Instalar ImageMagick
~ sudo -i
~ cd
~ apt-get install build-essential checkinstall && apt-get build-dep imagemagick -y
~ wget http://www.imagemagick.org/download/ImageMagick-6.8.7-7.tar.gz
~ tar xzvf ImageMagick-6.8.9-1.tar.gz
~ cd ImageMagick-6.8.9-1/
~ ./configure --prefix=/opt/imagemagick-6.8 && make
~ checkinstall

#Procesar PDF

Para crear las imagenes a ser colocadas en la revista digital se debe procesar un PDF (proporcionado por la CComercio) en tres diferentes tamaños de imagenes:

x90
x900
x1500

Para ello se deben seguir los siguientes pasos:

0. eliminar el contenido de las carpetas 'large', 'thumbs' y x900 ya que estos directorios son los que contendran las salidas del pdf a ser procesado.

1. colocar el PDF en la raiz de 'generar_imagenes_ccomercio' con el siguiente nombre: 'revista.pdf' (de no tener este nombre no será procesado).

y ejecutar los script.


2. Generar imagenes pequeñas: ejecutar el sh 'x90.sh' (esto puede tardar algunos minutos). la salida debe quedar en thumbs. 

3. Generar imagenes estandar: ejecutar el sh x900.sh. la salida debe quedar en la carpeta x900.

4. Generar imagenes Grandes: ejecutar el sh x1500.sh la salida debe quedar en la carpeta x1500.

5. Generar el pdf liviano: ejecutar el sh revista_web.sh. la salida debe quedar como revista_web.  
