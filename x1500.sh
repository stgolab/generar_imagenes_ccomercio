#!/bin/bash
blue=`tput setaf 4`
green=`tput setaf 2`
reset=`tput sgr0`
echo "${blue}°°°·.°·..·°¯°·._.·        © 𝕤𝕒𝕟𝕥𝕚𝕒𝕘𝕠𝕝𝕒𝕓         ·._.·°¯°·.·° .·°°°"
echo "°°°·.°·..·°¯°·._.· Script Convertir PDF a Imagenes tamaño x1500 ·._.·°¯°·.·° .·°°°"
echo ""
echo "${reset}Procesando... Por favor espere..."

convert -alpha remove -density 300 -shave 150x150 -resize x1500 -gravity center -scene 1 -intent relative -black-point-compensation -colorspace sRGB revista.pdf[0-43] large/%d.jpg

# pagina 1
#convert -alpha remove -density 300 +repage -crop 52%x100%+2700+0 -shave 130x160 +repage -resize x1500 -scene 1 -intent relative -black-point-compensation -colorspace sRGB revista.pdf[0] large/%d.jpg

# pagina 2
#convert -alpha remove -density 300 +repage -crop 52%x100%-20+0 -shave 150x170 +repage -resize x1500 -scene 2 -intent relative -black-point-compensation -colorspace sRGB revista.pdf[1] large/%d.jpg

# pagina 6
# convert -alpha remove -density 300 -shave 230x180 -resize x1500 -gravity center -scene 6 -intent relative -black-point-compensation -colorspace sRGB revista.pdf[5] large/%d.jpg

 #pagina 7
#convert -alpha remove -density 300 -shave 160x160 -resize x1500 -gravity center -scene 7 -intent relative -black-point-compensation -colorspace sRGB revista.pdf[6] large/%d.jpg

# pagina 43
#convert -alpha remove -density 300 +repage -crop 52%x100%+2700+0 -shave 150x210 +repage -resize x1500 -scene 43 -intent relative -black-point-compensation -colorspace sRGB revista.pdf[42] large/%d.jpg

# pagina 44
#convert -alpha remove -density 300 +repage -crop 52%x100%-20+0 -shave 130x160 +repage -resize x1500 -scene 44 -intent relative -black-point-compensation -colorspace sRGB revista.pdf[43] large/%d.jpg
